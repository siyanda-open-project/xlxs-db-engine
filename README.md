# XLSX DB Engine

[![license](https://img.shields.io/badge/license-Apache--2.0-green.svg)](https://tldrlegal.com/license/apache-license-2.0-(apache-2.0))


Excel parser/builder that can add/read data from the MongoDB database that relies on [js-xlsx](https://github.com/SheetJS/js-xlsx).


## Usage Inside Controllers/Services

### Examples

2. Building a xlsx

```js
const xlsxService = require('../generator/generator.service')

const data = [[1, 2, 3], [true, false, null, 'sheetjs'], ['foo', 'bar', new Date('2014-02-19T14:30Z'), '0.3'], ['baz', null, 'qux']];
const buffer = xlsxService.build([{name: "mySheetName", data: data}]); // Returns a buffer
```

  * Custom column width
```js
const xlsxService = require('../generator/generator.service')

const data = [[1, 2, 3], [true, false, null, 'sheetjs'], ['foo', 'bar', new Date('2014-02-19T14:30Z'), '0.3'], ['baz', null, 'qux']]
const options = {'!cols': [{ wch: 6 }, { wch: 7 }, { wch: 10 }, { wch: 20 } ]};

const buffer = xlsxService.build([{name: "mySheetName", data: data}], options); // Returns a buffer
```

  * Spanning multiple rows `A1:A4` in every sheets
```js
const xlsxService = require('../generator/generator.service')

const data = [[1, 2, 3], [true, false, null, 'sheetjs'], ['foo', 'bar', new Date('2014-02-19T14:30Z'), '0.3'], ['baz', null, 'qux']];
const range = {s: {c: 0, r:0 }, e: {c:0, r:3}}; // A1:A4
const options = {'!merges': [ range ]};

const buffer = xlsxService.build([{name: "mySheetName", data: data}], options); // Returns a buffer
```

  * Spanning multiple rows `A1:A4` in second sheet only
```js
const xlsxService = require('../generator/generator.service')

const dataSheet1 = [[1, 2, 3], [true, false, null, 'sheetjs'], ['foo', 'bar', new Date('2014-02-19T14:30Z'), '0.3'], ['baz', null, 'qux']];
const dataSheet2 = [[4, 5, 6], [7, 8, 9, 10], [11, 12, 13, 14], ['baz', null, 'qux']];
const range = {s: {c: 0, r:0 }, e: {c:0, r:3}}; // A1:A4
const sheetOptions = {'!merges': [ range ]};

const buffer = xlsxService.build([{name: "myFirstSheet", data: dataSheet1}, {name: "mySecondSheet", data: dataSheet2, options: sheetOptions}]); // Returns a buffer
```
_Beware that if you try to merge several times the same cell, your xlsx file will be seen as corrupted._


  * Using Primitive Object Notation
Data values can also be specified in a non-abstracted representation.

Examples:
```js
const rowAverage = [[{t:'n', z:10, f:'=AVERAGE(2:2)'}], [1,2,3];
const buffer = xlsxService.build([{name: "Average Formula", data: rowAverage}]);
```

Refer to [xlsx](https://sheetjs.gitbooks.io) documentation for valid structure and values:
- Cell Object: https://sheetjs.gitbooks.io/docs/#cell-object
- Data Types: https://sheetjs.gitbooks.io/docs/#data-types
- Format: https://sheetjs.gitbooks.io/docs/#number-formats

## Authors

**Siyanda Cele**

+ https://gitlab.com/siyanda-open-project/