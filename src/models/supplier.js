const mongoose = require('mongoose')

const addedByUser = function() {
	return this.createdBy === null
}
const supplierSchema = mongoose.Schema({
	name: { type: String, required: true },
	email: {
		type: String,
		required: true,
		unique: true,
		lowcase: true,
		match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
	},
	createdBy: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: function() {
			return this.isOnBoard
		}
	},
	contactNumber: { type: String, required: addedByUser },
	category: [{ type: String, required: true }],
	companyLogo: String,
	geometry: {
		formatted_address: { type: String },
		location: {
			lat: Number,
			lng: Number
		}
	},
	subcription: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Subcription',
		required: !addedByUser
	},
	stateOrProvince: { type: String, required: addedByUser },
	city: { type: String, required: addedByUser },
	country: { type: String, required: addedByUser },
	isOnBoard: { type: Boolean, default: addedByUser },
	assignedTo: {
		type: [
			{
				type: mongoose.Schema.Types.ObjectId,
				ref: 'User',
				required: addedByUser
			}
		],
		required: !addedByUser
	},
	addedBy: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: !addedByUser
	},
	description: { type: String, required: addedByUser },
	isActive: { type: Boolean, default: addedByUser },
	team: [
		{
			member: {
				type: mongoose.Schema.Types.ObjectId,
				ref: 'User',
				required: !addedByUser
			},
			role: {
				type: String,
				enum: ['admin', 'member']
			},
			_id: false
		}
	],
	website_url: { type: String },
	isDeleted: { type: Boolean, default: false },
	created_at: { type: Date, default: Date.now },
	updated_at: { type: Date, default: Date.now }
})

module.exports = mongoose.model('Supplier', supplierSchema)
