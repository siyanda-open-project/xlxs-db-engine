const mongoose = require('mongoose')

const userSchema = mongoose.Schema({
	method: {
		type: String,
		enum: ['local', 'google', 'for_facebook', 'company_invite'],
		require: true
	},
	name: { type: String },
	surname: { type: String },
	local: { password: { type: String } },
	company_invite: {
		company: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Supplier',
			required: true
		}
	},
	google: { id: { type: String } },
	email: {
		type: String,
		required: true,
		unique: true,
		lowcase: true,
		match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
	},
	active: { type: Boolean, default: false },
	businessAccount: [
		{
			company: {
				type: mongoose.Schema.Types.ObjectId,
				ref: 'Supplier',
				required: true
			},
			role: { type: String, enum: ['admin', 'member'], required: true },
			_id: false
		}
	],
	blocked: {
		type: Boolean,
		default: false
	},
	isOnboard: {
		type: Boolean,
		default: false
	},
	verifyToken: String,
	invitationToken: String,
	passwordRecoveryToken: String,
	phoneNumber: String,
	occupation: String,
	employer: String,
	suburb: String,
	gender: String,
	stateOrProvince: String,
	city: String,
	country: String,
	profilePic: String,
	created_at: {
		type: Date,
		default: Date.now
	},
	update_at: {
		type: Date,
		default: Date.now
	}
})

module.exports = mongoose.model('User', userSchema)
