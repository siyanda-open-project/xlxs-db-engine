const Suppliers = require('../models/supplier')
const User = require('../models/user')
const generatorService = require('../generator/generator.service')
const { supplierParser } = require('../parsers/supplier')

exports.generateSuppliers = async (req, res, next) => {
    try {
        const userId = req.user.sub
        const user = await User.findById(userId)
        if (!req.file) throw new Error('No file was uploaded.')
        const file = req.file.buffer
        const parsedSuppliers = supplierParser(generatorService.parse(file))
        const suppliersEmails = parsedSuppliers.map(supplier => supplier.email)
        const availableSuppliers = await Suppliers.find({ email: { $in: suppliersEmails } })
        const availableSuppliersEmail = availableSuppliers.map(supplier => supplier.email)
        const newSuppliers = parsedSuppliers
            .filter(function(e) { return this.indexOf(e.email) < 0 }, availableSuppliersEmail)
            .map(supplier => ({
                    ...supplier,
                    assignedTo: user._id,
                    addedBy: user._id}))

        Suppliers.updateMany({ email: { $in: suppliersEmails } }, {
            $addToSet: {
                assignedTo: user._id
        }}, {safe: true})
        Suppliers.insertMany(newSuppliers)
        res.status(201).json({created: 'Successfully Genarated...'})
    } catch (error) {
        res.status(400).json({
            error: error.message
        })
    }
}