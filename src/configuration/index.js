module.exports = {
	JWT_SECRET: "", // Add Secret
	MONGODB_PASS: "", // Add MONGODB PASSWORD
	MONGODB_CONNECTION_STRING: ``, // Add MONGODB CONNECTION STRING
	SYSTEM_MIME_TYPE: ['application/vnd.ms-excel', 'application/vnd.ms-excel.addin.macroenabled.12',
		'application/vnd.ms-excel.sheet.binary.macroenabled.12', 'application/vnd.ms-excel.sheet.macroenabled.12', 
		'application/vnd.ms-excel.template.macroenabled.12', 'text/csv', 
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
		'application/vnd.openxmlformats-officedocument.spreadsheetml.template']
}