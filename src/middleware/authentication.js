const jwt = require('jsonwebtoken')
const { JWT_SECRET } = require('../configuration')
const User = require('../models/user')

const tokenAuthorization = async req => {
	if (!req.headers.authorization) throw new Error('No token provided.')
	const token = req.headers.authorization.split(' ')[1]
	const decoded = jwt.verify(token, JWT_SECRET)
	const user = await User.findById(decoded.sub)
	if (!user) throw new Error('Token Invalid.')
	if (!user.active || user.blocked)
		throw new Error('User is not active or blocked.')
	return decoded
}

exports.authorization = async (req, res, next) => {
	try {
		req.user = await tokenAuthorization(req)
		next()
	} catch (error) {
		res.status(401).json({
			message: 'Access denied. ' + error.message
		})
	}
}
