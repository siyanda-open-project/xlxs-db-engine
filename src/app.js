const express = require('express')
const app = express()
const morgan = require('morgan')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const supplierRoutes = require('./routes/supplier')
const { MONGODB_CONNECTION_STRING } = require('./configuration')

mongoose.connect(MONGODB_CONNECTION_STRING, {
	useNewUrlParser: true,
	useCreateIndex: true,
	useFindAndModify: false 
})

mongoose.Promise = global.Promise

if (!process.env.NODE_ENV === 'prod') {
	app.use(morgan('dev'))
}

app.use(bodyParser.urlencoded({
	extended: false
}))
app.use(bodyParser.json())

app.use((req, res, next) => {
	const origin = req.get('origin')
	res.header('Access-Control-Allow-Origin', origin)
	res.header('Access-Control-Allow-headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
	if (req.method === 'OPTIONS') {
		res.header('Access-Control-Allow-Methods', 'POST, PATCH')
		return res.status(200).json({})
	}
	next()
})

app.use('/api/supplier', supplierRoutes)
app.use((req, res, next) => {
	const error = new Error('404 Not found')
	error.status = 404
	next(error)
})

app.use((error, req, res, next) => {
	res.status(error.status || 403)
	res.json({
		error: {
			message: error.message
		}
	})
})

module.exports = app