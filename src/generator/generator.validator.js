const Joi = require('joi')
module.exports = supplierValidator = Joi.object().keys({
    name: Joi.string().required(),
    email: Joi.string().email().required(),
    category: Joi.string().required(),
    contactNumber: Joi.string().required(),
    stateOrProvince: Joi.string().required(),
    city: Joi.string().required(),
    country: Joi.string().required(),
})