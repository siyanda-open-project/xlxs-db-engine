const XLSX = require('xlsx')
const bufferFrom = require('buffer-from')
const { buildSheetFromMatrix, isString } = require('./generator.common')
const Workbook = require('./generator.workbook')

exports.parse = (mixed, options = {}) => {
	const workSheet = XLSX[isString(mixed) ? 'readFile' : 'read'](mixed, options)
	return Object.keys(workSheet.Sheets).map(name => {
		const sheet = workSheet.Sheets[name]
		return {
			category: name,
			data: XLSX.utils.sheet_to_json(sheet, {
				header: 1,
				raw: options.raw !== false
			})
		}
	})
}

exports.build = (worksheets, options = {}) => {
	const defaults = {
		bookType: 'xlsx',
		bookSST: false,
		type: 'binary'
	}
	const workBook = new Workbook()
	worksheets.forEach(worksheet => {
		const sheetName = worksheet.name || 'Sheet'
		const sheetOptions = worksheet.options || {}
		const sheetData = buildSheetFromMatrix(worksheet.data || [], {
			...options,
			...sheetOptions
		})
		workBook.SheetNames.push(sheetName)
		workBook.Sheets[sheetName] = sheetData
	})
	const excelData = XLSX.write(workBook, Object.assign({}, defaults, options))
	return excelData instanceof Buffer
		? excelData
		: bufferFrom(excelData, 'binary')
}