const passport = require("passport");
const JwtStrategy = require("passport-jwt").Strategy;
const { ExtractJwt } = require("passport-jwt");
const LocalStrategy = require("passport-local").Strategy;
const { JWT_SECRET } = require("./configuration");

const User = require("./models/user");

// JSON WEB TOKEN STRATEGY
passport.use(
	new JwtStrategy(
		{
			jwtFromRequest: ExtractJwt.fromHeader("Authorization"),
			secretOrKey: JWT_SECRET,
			ignoreExpiration: false
		},
		async (payload, done) => {
			try {
				// Find the user specified in token
				const user = await User.findById(payload.sub);
				// if user doesn't exist, handle it
				if (!user) {
					return done(null, false);
				}
				// Otherwise return the user,
				done(null, user);
			} catch (error) {
				done(error, false);
			}
		}
	)
);

passport.use(
	new LocalStrategy(
		{
			usernameField: "email"
		},
		async (email, password, done) => {
			try {
				const errorMessage = {
					message: "Your email or password is wrong"
				};
				// Find the user given the email
				const user = await User.findOne({
					email: email
				});

				// if not, handle it
				if (!user) {
					return done(errorMessage, false);
				}

				// Check if the password is correct
				const isMatch = await user.isValidPassword(password);
				if (!isMatch) {
					return done(errorMessage, false);
				}

				// Check if the user is correct
				if (!user.active) {
					return done(
						{
							message: "Please verify your email address to login"
						},
						false
					);
				}
				// Otherwise, return user
				done(null, user);
			} catch (error) {
				console.log(error);
				done(error, false);
			}
		}
	)
);
