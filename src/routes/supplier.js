const express = require('express')
const router = express.Router()
const multer = require('multer')
const { authorization } = require('../middleware/authentication')
const supplierController = require('../controllers/supplier')
const { SYSTEM_MIME_TYPE } = require('../configuration')
const storage = multer.memoryStorage()

const upload = multer({
    storage: storage, limits: {
        fileSize: 1024 * 1024 * 50
    },
    fileFilter: (req, file, cb) => {
        if (SYSTEM_MIME_TYPE.includes(file.mimetype)) {
            cb(null, true)
        } else {
            cb(new Error('File type not allowed.'), false)
        }
    }
})

router.post('/', authorization, upload.single('suppliers'), supplierController.generateSuppliers)

module.exports = router