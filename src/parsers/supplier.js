const supplierValidator = require('../generator/generator.validator')
exports.supplierParser = (suppliersData) =>
    [].concat.apply([], suppliersData.map(sheet => {
    const category = sheet.category
    const suppliersDetails = sheet.data
    return suppliersDetails.slice(1).map((supplierDetails, index) => 
        supplierValidator.validate({
            name: supplierDetails[0],
            contactNumber: supplierDetails[1],
            email: supplierDetails[2].toLowerCase(),
            stateOrProvince: supplierDetails[3],
            city: supplierDetails[4],
            country: supplierDetails[5],
            category: category
        }, (err, value) => { 
            if (err) throw Error(err.message + ' at row number ' + (index + 1))
            return value
        }))
 }))